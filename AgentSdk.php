<?php namespace AgentSdk;

class AgentSdk {

    /**
     * Default gateway pointed.
     *
     * @var string
     */
    protected $gateway;

    /**
     * Agent app Id.
     *
     * If you don't have one, get that from contact "teepluss".
     *
     * @var string
     */
    protected $appId = null;

    /**
     * Secret to compare with app Id.
     *
     * @var string
     */
    protected $secret = null;

    /**
     * Endpoint of agent api.
     *
     * @var string
     */
    protected $endpoint = 'http://agent.order.to/api';

    /**
     * URL to redirecting.
     *
     * @var string
     */
    protected $redirectUrl = 'http://agent.order.to/redirect';

    /**
     * Gateway foreground return url.
     *
     * @var string
     */
    protected $returnUrl = null;

    /**
     * Gateway background return url.
     *
     * @var string
     */
    protected $verifyUrl = null;

    /**
     * Default gateway account.
     *
     * @var string
     */
    protected $account;

    /**
     * Gateway using sandbox.
     *
     * @var boolean
     */
    protected $sandbox = false;

    /**
     * CURL default options.
     *
     * @var array
     */
    public static $CURL_OPTS = array(
        CURLOPT_CONNECTTIMEOUT => 10,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT        => 60,
        CURLOPT_USERAGENT      => 'agent-0.1',
    );

    /**
     * SDK construct.
     *
     * @param array $params
     */
    public function __construct($params = array())
    {
        $this->initialize($params);
    }

    /**
     * Intialize config.
     *
     * @param  array $params
     * @return void
     */
    public function initialize($params)
    {
        if (count($params)) foreach ($params as $key => $val)
        {
            $method = 'set'.ucfirst($key);

            $this->$method($val);
        }
    }

    /**
     * Set app id.
     *
     * @param string $appId
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;

        return $this;
    }

    /**
     * Set secret.
     *
     * @param string $secret
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Set gateway.
     *
     * @param string $gateway
     */
    public function setGateway($gateway)
    {
        $this->gateway = ucfirst($gateway);

        return $this;
    }

    /**
     * Set gateway account.
     *
     * @param string $account
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Set agent endpoint.
     *
     * @param string $endpoint
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    /**
     * Set agent redirect url.
     *
     * @param string $redirectUrl
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;

        return $this;
    }

    /**
     * Set return url.
     *
     * @param string $returnUrl
     */
    public function setReturnUrl($returnUrl)
    {
        $this->returnUrl = $returnUrl;

        return $this;
    }

    /**
     * Set verify url.
     *
     * @param string $verifyUrl
     */
    public function setVerifyUrl($verifyUrl)
    {
        $this->verifyUrl = $verifyUrl;

        return $this;
    }

    /**
     * Set agent using sandbox.
     *
     * @param boolean $sandbox
     */
    public function setSandbox($sandbox)
    {
        $this->sandbox = $sandbox;

        return $this;
    }

    /**
     * Get app id.
     *
     * @return string
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * Get token to payment.
     *
     * @param  array  $args
     * @return string
     */
    public function getToken(array $args)
    {
        $url = $this->endpoint.'/request';

        $params = array_merge(array(
            'sandbox'     => $this->sandbox,
            'account'     => $this->account,
            'gateway'     => $this->gateway,
            'returnUrl'   => $this->returnUrl,
            'verifyUrl'   => $this->verifyUrl,
            'language'    => 'EN',
            'currency'    => 'USD',
            'invoice'     => null,
            'description' => null,
            'amount'      => 0,
            // Addition use for non-standard gateway.
            'addtion'     => array()
        ), $args);

        $response = $this->api('/request', $params);

        if ( ! isset($response['token']) or ! is_numeric($response['token']))
        {
            throw new AgentException('Cannot request token payment.', 400);
        }

        return isset($response['token']) ? $response['token'] : null;
    }

    /**
     * Get redirecting url.
     *
     * @param  string $token
     * @return string
     */
    public function getRedirectUrl($token)
    {
        return $this->redirectUrl.'/'.$token;
    }

    /**
     * Call api to verify post back data.
     *
     * @param  array   $input
     * @param  integer $amount
     * @return mixed
     */
    public function verify($input)
    {
        $verified = $this->api('/verify', $input);

        return $verified;
    }

    /**
     * Wrap with "Process" class.
     *
     * @param  array   $input
     * @return Process
     */
    public function process($input)
    {
        return new Process($input);
    }

    /**
     * Push data to endpoint.
     *
     * @param  string $path
     * @param  array  $args
     * @return mixed
     */
    protected function api($path, array $args)
    {
        $uri = $this->endpoint.'/'.ltrim($path, '/');

        $args = array_merge(array(
            'appId'  => $this->appId,
            'secret' => $this->secret
        ), $args);

        return $this->makeRequest($uri, $args);
    }

    /**
     * Makes an HTTP request. This method can be overridden by subclasses if
     * developers want to do fancier things or use something other than curl to
     * make the request.
     *
     * @param  string $url
     * @param  array  $args
     * @param  CURL   $ch
     * @return string
     */
    protected function makeRequest($url, array $args, $ch = null)
    {
        if ( ! $ch)
        {
            $ch = curl_init();
        }

        $opts = self::$CURL_OPTS;

        $opts[CURLOPT_URL] = $url;

        $opts[CURLOPT_POSTFIELDS] = http_build_query($args, null, '&');

        // Ignore verify SSL.
        $opts[CURLOPT_SSL_VERIFYHOST] = 0;
        $opts[CURLOPT_SSL_VERIFYPEER] = 0;


        // disable the 'Expect: 100-continue' behaviour. This causes CURL to wait
        // for 2 seconds if the server does not support this header.
        if (isset($opts[CURLOPT_HTTPHEADER]))
        {
            $existing_headers = $opts[CURLOPT_HTTPHEADER];
            $existing_headers[] = 'Expect:';
            $opts[CURLOPT_HTTPHEADER] = $existing_headers;
        }
        else
        {
            $opts[CURLOPT_HTTPHEADER] = array('Expect:');
        }

        curl_setopt_array($ch, $opts);
        $result = curl_exec($ch);

        $json = json_decode($result, true);

        //sd($url, $opts, $result, $json);

        // Handle error.
        if (isset($json['error']))
        {
            $error = $json['error'];

            $message = json_encode($error);

            $code = $error['code'];

            throw new AgentException($message, $error['code']);
        }

        return ($json) ?: $result;
    }

}
