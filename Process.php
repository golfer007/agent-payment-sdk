<?php namespace AgentSdk;

class Process {

    /**
     * Input from background response.
     *
     * @var array
     */
    protected $input;

    /**
     * Construct
     *
     * @param array $input
     */
    public function __construct($input)
    {
        // Remove CSRF token.
        if (isset($input['_token']))
        {
            unset($input['_token']);
        }

        // Change dump data to array.
        if (isset($input['dump']))
        {
            $input['dump'] = ($this->isJson($input['dump'])) ? json_decode($input['dump'], true) : unserialize($input['dump']);
        }

        $this->input = $input;

        return $this;
    }

    /**
     * Get value with key.
     *
     * @param  string $key
     * @return mixed
     */
    public function get($key = '')
    {
        return isset($this->input[$key]) ? $this->input[$key] : $this->input;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->get('status');
    }

    /**
     * Get data returned as object.
     *
     * @return object
     */
    public function getReturned()
    {
        return (object) $this->input;
    }

    /**
     * Checking status.
     *
     * @param  string  $status
     * @return boolean
     */
    public function is($status)
    {
        return strcmp($this->get('status'), $status) === 0;
    }

    /**
     * Compare success status.
     *
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->is('success');
    }

    /**
     * Compare failed status.
     *
     * @return boolean
     */
    public function isFailed()
    {
        return $this->is('failed');
    }

    /**
     * Compare pending status.
     *
     * @return boolean
     */
    public function isPending()
    {
        return $this->is('pending');
    }

    /**
     * Compare unequal status.
     *
     * @return boolean
     */
    public function isUnequal()
    {
        return $this->is('unequal');
    }

    /**
     * Compare not success status.
     *
     * @return boolean
     */
    public function isNotSuccess()
    {
        return ! $this->isSuccess();
    }

    /**
     * Checking is Json format.
     *
     * @param  string  $string
     * @return boolean
     */
    private function isJson($string)
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }

}