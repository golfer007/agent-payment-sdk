<?php namespace AgentSdk;

class AgentException extends \Exception {

    /**
     * Error messages.
     *
     * @var array
     */
    protected $error;

    /**
     * Construct.
     *
     * @param string  $message  json
     * @param integer $code
     * @param mixed   $previous
     */
    public function __construct($message, $code, $previous = null)
    {
        $this->error = json_decode($message, true);

        if ( ! is_object($this->error))
        {
            $this->error['message'] = $message;
        }

        parent::__construct($this->error['message'], $code, $previous);
    }

    /**
     * Get error key.
     *
     * @param  string $key
     * @return mixed
     */
    public function get($key)
    {
        return (isset($this->error[$key])) ? $this->error[$key] : null;
    }

    /**
     * Megic method to call get.
     *
     * @param  string $method
     * @param  array  $args
     * @return mixed
     */
    public function __call($method, $args = array())
    {
        if (preg_match('/^get([a-zA-Z]+)/', $method, $matches))
        {
            $key = strtolower($matches[1]);

            return $this->get($key);
        }
    }

}